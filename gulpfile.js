var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    git = require('gulp-git'),
    codePaths = ['app.js'];

gulp.task('default', ['min', 'watch']);

gulp.task('min', function () {
    return gulp.src(codePaths)
        .pipe(concat('sp-bubble.min.js'))
        .pipe(uglify({
            compress: true
        }))
        .pipe(gulp.dest('dist'))
        .pipe(git.add());
});

gulp.task('watch', function () {
    gulp.watch(codePaths, ['min']);
});