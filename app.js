angular.module('spBubble', []).config(['$provide', function ($provide) {
    var provider = $provide.service('Bubble', ['$q', '$rootScope', '$templateCache', '$http', '$controller', '$compile', function ($q, $rootScope, $templateCache, $http, $controller, $compile) {
        var self = this,
            _bubbles = {};

        self.show = show;
        self.hide = hide;

        /**
         * @typedef {Object} BubbleOptions
         * @property {Array/Function} controller
         * @property {String} [template]
         * @property {String} [templateUrl]
         * @property {Number=Infinity} [hideTime] hide after X ms
         * @property {Object} position
         * @property {Element=document.body} nextTo
         * @property {Boolean=false} [fixed]
         * @property {String} [showClass]
         * @property {String} [hideClass]
         * @property {String} [id]
         */

        /**
         * Show new bubble
         * @public
         *
         * @param {BubbleOptions} options
         *
         * @return {Promise}
         */
        function show(options) {
            var deferred = $q.defer();

            options.deferred = deferred;
            options.hideTime = options.hideTime || Infinity;
            options.nextTo = options.nextTo || document.body;

            _getTemplate(options)
                .then(function (template) {
                    options.template = template;
                    var $element = _createElement(options),
                        nextToPosition = {top: 0, left: 0},
                        css = {
                            position: options.fixed ? 'fixed' : 'absolute'
                        };

                    var element = options.nextTo;
                    while (element) {
                        if (element == document.body) break;

                        nextToPosition.top += element.offsetTop;
                        nextToPosition.left += element.offsetLeft;

                        element = element.offsetParent;
                    }

                    if (typeof options.position.left == 'number') {
                        css['left'] = (options.position.left + nextToPosition.left) + 'px';
                    } else if (typeof options.position.right == 'number') {
                        css['left'] = (options.position.right + nextToPosition.left + options.nextTo.clientWidth) + 'px';
                    }

                    if (typeof options.position.top == 'number') {
                        css['top'] = (options.position.top + nextToPosition.top) + 'px';
                    } else if (typeof options.position.bottom == 'number') {
                        css['top'] = (options.position.bottom + nextToPosition.top + options.nextTo.clientHeight) + 'px';
                    }

                    angular.element(document.body).append($element.css(css));

                    if (options.showClass) {
                        $element.addClass(options.showClass);
                    }

                    if (options.hideTime < Infinity) {
                        setTimeout(function () {
                            _hide($element, options);
                        }, options.hideTime);
                    }

                    if (options.id) {
                        _bubbles[options.id] = {$element: $element, options: options};
                    }
                })
                .catch(deferred.reject);

            return deferred.promise;
        }

        /**
         * Hide bubble by id
         * @public
         *
         * @param {String} id
         */
        function hide(id) {
            var bubble = _bubbles[id];
            if (!bubble) return;

            return _hide(bubble.$element, bubble.options);
        }

        /**
         * Hide bubble
         * @private
         *
         * @param {Element} $element
         * @param {BubbleOptions} options
         * @param {Object} options.deferred
         */
        function _hide($element, options) {
            if (options.hideClass) {
                $element.addClass(options.hideClass);
            }

            function remove() {
                $element.remove();
                options.resolve.$scope.$destroy();
                options.deferred.resolve();
                if (options.id) {
                    delete _bubbles[options.id];
                }
            }

            if (options.onHide) {
                return options.onHide($element, remove);
            }

            remove();
        }

        /**
         * Get template by templateUrl or return options.template
         * @private
         *
         * @param {BubbleOptions} options
         *
         * @return {Promise}
         * @resolve {String} template
         */
        function _getTemplate(options) {
            if (options.template) {
                return $q.resolve(options.template);
            }

            return $http.get(options.templateUrl, {
                cache: $templateCache
            }).then(function (resp) {
                return resp.data;
            });
        }

        /**
         * Create bubble element
         * @private
         *
         * @param {BubbleOptions} options
         *
         * @return {Element}
         */
        function _createElement(options) {
            return _createScopeAndController(options, provider.wrapperElement.clone().html(options.template));
        }

        /**
         * Create angular scope and run the controller on it
         * @private
         *
         * @param {BubbleOptions} options
         * @param {Element} $element
         *
         * @return {Element}
         */
        function _createScopeAndController(options, $element) {
            options.controller = options.controller || angular.noop;
            options.resolve = options.resolve || {};
            options.resolve.$scope = $rootScope.$new();
            options.resolve.$element = $element;
            options.resolve.hide = function () {
                setTimeout(function () {
                    _hide($element, options);
                }, 0);
            };

            $controller(options.controller, options.resolve);
            $compile($element)(options.resolve.$scope);

            return $element;
        }
    }]);

    /**
     * When show bubble the service wrapper the html with this (clone) element
     *
     * @type {Element} AngularJS element
     * @example
     * angular.element(document.createElement('div')).addClass('example')
     */
    provider.wrapperElement = angular.element(document.createElement('div')).addClass('sp-bubble');
}]);
