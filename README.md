#sp-bubble
service for angularjs to create bubbles

##example
```
bubble.show({
    controller: ['$scope', function ($scope) {
        $scope.test = function () {
            window.alert('test');
        }
    }],
    position: {
        top: 0,
        left: 0
    },
    template: '<button ng-click="test()">test</button>',
    nextTo: document.querySelector('#after')
}).then(function () {
    console.log('bubble removed');
});
```

##commit
before any commit do `gulp` (to create dist file)